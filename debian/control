Source: rust-synstructure
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-proc-macro2-1+proc-macro-dev <!nocheck>,
 librust-proc-macro2-1-dev <!nocheck>,
 librust-quote-1+proc-macro-dev <!nocheck>,
 librust-quote-1-dev <!nocheck>,
 librust-syn-1+clone-impls-dev <!nocheck>,
 librust-syn-1+derive-dev <!nocheck>,
 librust-syn-1+extra-traits-dev <!nocheck>,
 librust-syn-1+parsing-dev <!nocheck>,
 librust-syn-1+printing-dev <!nocheck>,
 librust-syn-1+proc-macro-dev <!nocheck>,
 librust-syn-1+visit-dev <!nocheck>,
 librust-unicode-xid-0.2+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Ximin Luo <infinity0@debian.org>,
 Wolfgang Silbermayr <wolfgang@silbermayr.at>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/synstructure]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/synstructure

Package: librust-synstructure-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-proc-macro2-1-dev,
 librust-quote-1-dev,
 librust-syn-1+clone-impls-dev,
 librust-syn-1+derive-dev,
 librust-syn-1+extra-traits-dev,
 librust-syn-1+parsing-dev,
 librust-syn-1+printing-dev,
 librust-syn-1+visit-dev,
 librust-unicode-xid-0.2+default-dev
Recommends:
 librust-synstructure+proc-macro-dev (= ${binary:Version})
Provides:
 librust-synstructure-0-dev (= ${binary:Version}),
 librust-synstructure-0.12-dev (= ${binary:Version}),
 librust-synstructure-0.12.3-dev (= ${binary:Version})
Description: Helper methods and macros for custom derives - Rust source code
 This package contains the source for the Rust synstructure crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-synstructure+proc-macro-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-synstructure-dev (= ${binary:Version}),
 librust-proc-macro2-1+proc-macro-dev,
 librust-quote-1+proc-macro-dev,
 librust-syn-1+proc-macro-dev
Provides:
 librust-synstructure+default-dev (= ${binary:Version}),
 librust-synstructure-0+proc-macro-dev (= ${binary:Version}),
 librust-synstructure-0+default-dev (= ${binary:Version}),
 librust-synstructure-0.12+proc-macro-dev (= ${binary:Version}),
 librust-synstructure-0.12+default-dev (= ${binary:Version}),
 librust-synstructure-0.12.3+proc-macro-dev (= ${binary:Version}),
 librust-synstructure-0.12.3+default-dev (= ${binary:Version})
Description: Helper methods and macros for custom derives - feature "proc-macro" and 1 more
 This metapackage enables feature "proc-macro" for the Rust synstructure crate,
 by pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "default" feature.
